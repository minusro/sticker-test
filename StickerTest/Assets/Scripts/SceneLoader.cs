﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour 
{
	[SerializeField] private string[] SceneNames;

	private void Awake() 
	{
		for (var i = 0; i < SceneNames.Length; i++)
		{
			SceneManager.LoadScene(SceneNames[i], LoadSceneMode.Additive);
		}
	}
}
