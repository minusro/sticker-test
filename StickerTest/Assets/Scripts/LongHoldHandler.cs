﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class LongHoldHandler : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
	public Action<PointerEventData> OnLongHold;
	public Action<PointerEventData> OnLongRelease;

	private PointerEventData pointerDownEventData;
	private float lastTapTime;
	private bool wasHeld;
	
	public const float HOLD_TIME = 0.3f;

	private void Update()
	{
		if (!wasHeld && pointerDownEventData != null && Time.time > lastTapTime + HOLD_TIME)
		{
			wasHeld = true;
			if (OnLongHold != null)
			{
				OnLongHold(pointerDownEventData);
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		wasHeld = false;
		lastTapTime = Time.time;
		pointerDownEventData = eventData;
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (wasHeld && OnLongRelease != null)
		{
			OnLongRelease(pointerDownEventData);
		}
		pointerDownEventData = null;
	}
}
