﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Element : MonoBehaviour 
{
	protected virtual bool IgnoreRaycast { set { } }

	private const float SPAWN_DISTANCE = 5f;

	public Element MakeSpawnerInstance(Transform parent)
	{
		var newObject = Instantiate(this.gameObject, parent);
		var holdHandler = newObject.AddComponent<LongHoldHandler>();
		holdHandler.OnLongHold += SpawnDraggableInstance;
		holdHandler.OnLongRelease += Cleanup;
		return newObject.GetComponent<Element>();
	}

	private void SpawnDraggableInstance(PointerEventData eventData)
	{
		var menuElement = eventData.pointerPress.GetComponent<Element>();
		var newParent = menuElement.GetComponentInParent<Canvas>().transform;
		var newElement = Instantiate(this.gameObject, newParent).GetComponent<Element>();
		newElement.gameObject.AddComponent<DragHandler>();
		newElement.transform.position = GetSpawnPosition(eventData);
		newElement.IgnoreRaycast = true;	
		eventData.pointerDrag = newElement.gameObject;
	}

	private void Cleanup(PointerEventData eventData)
	{
		if (!eventData.dragging)
		{
			Destroy(eventData.pointerDrag);
		}
	}
	
	public void Finalize()
	{
		IgnoreRaycast = false;
		Destroy(GetComponent<DragHandler>());
		Destroy(GetComponent<LongHoldHandler>());
	}

	private Vector3 GetSpawnPosition(PointerEventData eventData)
	{
		var stickerCenter = eventData.pointerPress.transform.position;
		var pointerPosition = new Vector3(eventData.position.x, eventData.position.y);
		var direction = (pointerPosition - stickerCenter).normalized;
		return stickerCenter + SPAWN_DISTANCE * direction;
	}
}
