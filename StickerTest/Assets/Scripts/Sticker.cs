﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Sticker : Element
{
	[SerializeField] private Image Image;
	[SerializeField] private Color Tint;

	protected override bool IgnoreRaycast 
	{
		set 
		{
			Image.raycastTarget = !value; 
		} 
	}

	private void Awake()
	{
		Image.color = Tint;
	}
}
