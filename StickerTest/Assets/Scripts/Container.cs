﻿using UnityEngine;
using UnityEngine.EventSystems;

public class Container : MonoBehaviour, IDropHandler
{
	public void OnDrop(PointerEventData eventData)
	{
		var element = eventData.pointerDrag.GetComponent<Element>();
		if (element != null)
		{
			element.Finalize();
			element.transform.SetParent(this.transform);
			OnDrop(element);
		}
	}

	public virtual void OnDrop(Element element) { }
}
