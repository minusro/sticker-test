﻿using System.Collections.Generic;
using UnityEngine;

public class StickerMenu : Container
{
	[SerializeField] private List<Sticker> AvailableStickers;
	[SerializeField] private Transform ScrollContent;

	private void Awake()
	{
		for (var i = 0; i < AvailableStickers.Count; i++)
		{
			AvailableStickers[i].MakeSpawnerInstance(ScrollContent);
		}
	}

	public override void OnDrop(Element element)
	{
		Destroy(element.gameObject);
	}
}
