﻿using System.Collections.Generic;
using UnityEngine;

public class CanvasContainer : Container
{
	[SerializeField] private List<Sticker> StickersInUse;

	public override void OnDrop(Element element)
	{
		StickersInUse.Add((Sticker) element);
	}
}
